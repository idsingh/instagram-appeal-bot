## Pre-requisites

- Node.JS 10
- tor

## Configuring Tor
On Debian/Ubuntu you can install and run a relatively up to date Tor with.
```sh
apt install tor
```

On Mac you can install and run a relatively up to date Tor with.
```sh
brew install tor
```

```sh
tor --hash-password giraffe
```
Copy the hashed password into torrc file in project directory in front of HashedControlPassword variable

## Configuring the application
- Clone or download the code
- Run the below scripts to configure the project 
```sh
npm i
```
- Optional (Edit the variables in config.js as per your convenience)
- Configure default appeal reasons if you don't want to put in csv. Any one appeal reason will be picked at random.

## Configuring the instagram accounts
- You can put comma separated values in data.csv in the format (name,email,username,mobileNumber,appealReason) from second line onwards

## Starting the application
- Clone or download the code
- Run the below scripts to configure the project 
```sh
cd <Project Directory>
tor -f ./torrc
npm start
```
