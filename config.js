module.exports = {
    delay : 5, // Seconds
    showBrowser : true, // To view progress in real time
    proxy : ['socks5://127.0.0.1:9050'], // Use Proxy with it (Optional)
    appealReasons : [ // Default Appeal Reasons, Appeal Reasons would be picked at random
        `Dear Concern,
        
        My account has been deactivated due to some mistake as this is a completely genuine personal account that
        i use for my instagram activity. Kindly re-activate it as i promise to abide by the rules and regulations 
        of instagram in future.

        Thanks and have a nice day        
        `,
        `Dear Concern,
        
        My account has been deactivated due to some mistake as I have not violated any legal regulation of this platform 
        and abide by the rules and regulations of instagram in future.

        Thanks and have a nice day        
        `,
        `Dear Concern,
        
        My account has been deactivated by mistake. I have provided my information with this appeal and i am willing to provide
        any info required in future for reactivation of this account

        Thanks and have a nice day        
        `
    ],
    torPassword : 'giraffe'
}