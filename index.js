/**
 * Require the puppeteer library.
 */
const apify = require("apify");
const randomUA = require("modern-random-ua");
const os = require("os");
const config = require("./config");
const csvToJson = require("csvtojson");
var tr = require("tor-request");
tr.TorControlPort.password = config.torPassword;
function renewProxy() {
  return new Promise((resolve, reject) => {
    tr.newTorSession((err) => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
}
/**
 * Require the cheerio library.
 */
let browser;
async function delay(ms) {
  return await new Promise((res) => setTimeout(res, ms * 1000));
}
function parseJSON(json, throwError) {
  try {
    return JSON.parse(json);
  } catch (e) {
    if (throwError) {
      throw e;
    }
  }
}

const Selectors = {
  form: "#MAIN_CONTENT_ID form",
  name: 'input[name="name"]',
  email: 'input[name="email"]',
  username: 'input[name="instagram_username"]',
  mobileNumber: 'input[name="mobile_number"]',
  appealReason: 'textarea[name="appeal_reason"]',
  submit: 'button[type="submit"]',
  modalText: '[role="dialog"] .pam',
  cookieBannerBtn: '[data-cookiebanner="accept_button"]',
};

/**
 * Inside the main function we'll place all the required code
 * that will be used in the scraping process.
 * The reason why we create an async function is to use
 * the power of async programming  that comes with puppeteer.
 */
async function main() {
  try {
    /**
     * Launch Chromium. By setting `headless` key to false,
     * we can see the browser UI.
     */
    const appeals = await csvToJson().fromFile("./data.csv");
    for (let appeal of appeals) {
      console.log("Generating New proxy");
      await renewProxy();
      await delay(2);
      console.log(
        "Trying to submit appeal for name - " +
          appeal.name +
          " email - " +
          appeal.email +
          " - username - " +
          appeal.username
      );
      const options = {
        userAgent: randomUA.generate(),
        args: [
          "--no-sandbox",
          "--disable-setuid-sandbox",
          "--disable-infobars",
          "--window-position=0,0",
          "--ignore-certifcate-errors",
          "--ignore-certifcate-errors-spki-list",
          ...(config.proxy
            ? [
                "--proxy-server=" +
                  config.proxy[Math.floor(Math.random() * config.proxy.length)],
              ]
            : {}),
          '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"',
        ], //'--incognito'],

        stealth: true,
        headless: !config.showBrowser,
        userDataDir: `${os.tmpdir()}/iab-${Math.random()}`,
      };
      browser = await apify.launchPuppeteer(options);

      /**
       * Create a new page.
       */
      const page = await browser.newPage();
      await page.goto(`https://help.instagram.com/contact/606967319425038`);
      if (await page.$(Selectors.cookieBannerBtn)) {
        console.log("Has Cookie Banner");
        await page.click(Selectors.cookieBannerBtn);
      }
      await page.waitForSelector("#MAIN_CONTENT_ID form");

      await page.type(Selectors.name, appeal.name);
      await page.type(Selectors.email, appeal.email);
      await page.type(Selectors.username, appeal.username);
      appeal.mobileNumber &&
        (await page.type(Selectors.mobileNumber, appeal.mobileNumber));
      appeal.appealReason =
        appeal.appealReason ||
        config.appealReasons[
          Math.floor(Math.random() * config.appealReasons.length)
        ];
      await page.type(Selectors.appealReason, appeal.appealReason);
      console.log("Appeal Reason - " + appeal.appealReason);
      await page.click(Selectors.submit);
      try {
        await page.waitForSelector(Selectors.modalText);

        console.log(
          "Message from instagram - " +
            (await page.$eval(Selectors.modalText, (el) => el.innerText))
        );
        console.log("Waiting for " + (config.delay || 5) + " seconds");
        config.delay && (await delay(config.delay));
        
      } catch (e) {
        console.log("Request Timed out", { e });
      }
       await browser.close();
    }
  } catch (e) {
    console.error("Error in main process", { e });
  }
}

main();
